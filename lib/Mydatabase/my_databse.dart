import 'dart:io';
import 'package:flutter/services.dart';
import 'package:path_provider/path_provider.dart';
import 'package:sqflite/sqflite.dart';
import 'package:path/path.dart';
import 'package:untitled2/model/city_model.dart';

class MyDatabase {
  Future<Database> initDatabase() async {
    Directory appDocDir = await getApplicationDocumentsDirectory();
    String databasePath = join(appDocDir.path, 'api_db.db');
    return await openDatabase(databasePath);
  }

  Future<bool> copyPasteAssetFileToRoot() async {
    Directory documentsDirectory = await getApplicationDocumentsDirectory();
    String path = join(documentsDirectory.path, "api_db.db");

    if (FileSystemEntity.typeSync(path) == FileSystemEntityType.notFound) {
      ByteData data =
      await rootBundle.load(join('assets/database', 'api_db.db'));
      List<int> bytes =
      data.buffer.asUint8List(data.offsetInBytes, data.lengthInBytes);
      await File(path).writeAsBytes(bytes);
      return true;
    }
    return false;
  }

  Future<List<Map<String, Object?>>> getDataFromuser() async {
    Database db = await initDatabase();
    List<Map<String, Object?>> data = await db.rawQuery(      'SELECT Tbl_User.Name as UserName,'
        'Tbl_City.Name as CityName,'
        'UserID,'
        'Dob '
        'FROM Tbl_User '
        'INNER JOIN Tbl_City '
        'On '
        'Tbl_User.CityId = Tbl_City.CityID');
    return data;
  }
  Future<List<CityModel>> getCities() async {
    Database db = await initDatabase();
    List<CityModel> CityList=[];
    List<Map<String, Object?>> data = await db.rawQuery( "select * from Tbl_City");
    for (var i in data){
      CityModel model=CityModel();
      model.Name=i['Name'].toString();
      model.CityID=int.parse(i['CityID'].toString());
      CityList.add(model);
    }
    print(CityList);
    return CityList;
  }

  Future<void> addUser(map) async {
    print(map);
    Database db=await initDatabase();
    await db.insert('Tbl_User', map);
  }

  Future<void> deleteUser(id) async{
    Database db=await initDatabase();
    await db.delete('Tbl_User',whereArgs: [id],where: 'UserID = ?');
  }
  Future<void> updateUser(id,map) async{
    Database db=await initDatabase();
    await db.update('Tbl_User',map,whereArgs: [id],where: 'UserID = ?');
  }
}
