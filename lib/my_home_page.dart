import 'package:flutter/material.dart';
import 'package:untitled2/Mydatabase/my_databse.dart';
import 'package:untitled2/add_user.dart';

class MyHomePage extends StatefulWidget {
  const MyHomePage({Key? key}) : super(key: key);

  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  MyDatabase db = MyDatabase();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          Navigator.push(context, MaterialPageRoute(builder: (context) => AddUser(),)).then((value) {
            setState(() {
              db.getDataFromuser();
            });
          },);
        },
      ),
      body: FutureBuilder(
        builder: (context, snapshot1) {
          if (snapshot1.hasData) {
            return FutureBuilder(
              builder: (context, snapshot) {
                if (snapshot.hasData) {
                  return ListView.builder(
                    itemBuilder: (context, index) {
                      return InkWell(
                      onTap: () {
                        Navigator.push(context, MaterialPageRoute(builder: (context) => AddUser(map: snapshot.data![index]),)).then((value) {
                          setState(() {
                            db.getDataFromuser();
                          });
                        },);
                      },
                        child: Card(
                          child: Row(
                            children: [
                              Column(
                                children: [
                                  Text(snapshot.data![index]['UserName']
                                      .toString()),
                                  Text(snapshot.data![index]['UserID']
                                      .toString()),
                                  Text(snapshot.data![index]['CityName']
                                      .toString()),
                                  Text(snapshot.data![index]['Dob'].toString()),
                                ],
                              ),
                              Spacer(),
                              InkWell(
                                onTap: () {
                                  db
                                      .deleteUser(
                                      snapshot.data![index]['UserID'])
                                      .then(
                                        (value) {
                                      setState(() {
                                        db.getDataFromuser();
                                      });
                                    },
                                  );
                                },
                                child: Icon(
                                  Icons.delete,
                                  color: Colors.redAccent,
                                ),
                              ),
                            ],
                          ),
                        ),
                      );
                    },
                    itemCount: snapshot.data!.length,
                  );
                } else {
                  return CircularProgressIndicator();
                }
              },
              future: db.getDataFromuser(),
            );
          } else {
            return CircularProgressIndicator();
          }
        },
        future: db.copyPasteAssetFileToRoot(),
      ),
    );
  }
}
