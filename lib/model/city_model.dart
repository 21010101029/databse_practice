class CityModel{
  int? _CityID;

  int? get CityID => _CityID;

  set CityID(int? value) {
    _CityID = value;
  }
  String? _Name;

  String? get Name => _Name;

  set Name(String? value) {
    _Name = value;
  }
}