import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:untitled2/Mydatabase/my_databse.dart';
import 'package:untitled2/model/city_model.dart';

class AddUser extends StatefulWidget {
  Map<String,dynamic>? map;
  AddUser({this.map});

  @override
  State<AddUser> createState() => _AddUserState();
}

class _AddUserState extends State<AddUser> {
  TextEditingController _nameController=TextEditingController();
  TextEditingController _dateController=TextEditingController();
  MyDatabase db = MyDatabase();
  CityModel? _selected;
  bool isCityget=true;


  @override
  void initState() {
    _nameController.text = widget.map == null ? '' : widget.map!['UserName'].toString();
    _dateController.text = widget.map == null ? '' : widget.map!['Dob'].toString();
    super.initState();
  }
  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        body: Column(
          children: [
            TextFormField(
              controller: _nameController,
              decoration: InputDecoration(labelText: "Name"),
            ),
            FutureBuilder<List<CityModel>>(
              builder: (context, snapshot) {
                if (snapshot.hasData) {
                  if(isCityget){
                    _selected = snapshot.data![1];
                    isCityget = false;
                  }
                  return DropdownButton(
                    value: _selected,
                    items: snapshot.data!.map((e) {
                      return DropdownMenuItem(
                          value: e,
                          child: Text(
                            e.Name.toString(),
                          ));
                    }).toList(),
                    onChanged: (value) {
                      setState(() {
                        _selected = value;
                      });
                    },
                  );
                }
                else{
                  return Container();
                }
              },
              future: isCityget?MyDatabase().getCities():null,
            ),
            TextFormField(
              controller: _dateController,
              decoration: InputDecoration(labelText: "Age"),
            ),
            TextButton(
                onPressed: () async {
                  Map<String,dynamic> map = {};
                  map["Name"] = _nameController.text.toString();
                  map["Dob"] = _dateController.text;
                  map["CityId"] = int.parse(_selected!.CityID.toString());

                  if(widget.map!=null){
                    await db.updateUser(widget.map!['UserID'], map).then((value) => Navigator.pop(context));
                  }
                  else{
                    await db.addUser(map).then((value) => Navigator.pop(context));
                  }

                },
                child: Icon(
                  Icons.done,
                  size: 50,
                )),
          ],
        ),
      ),
    );
  }
}
